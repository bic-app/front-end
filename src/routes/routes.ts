import { createWebHistory, createRouter } from "vue-router";
import { RouteRecordRaw } from "vue-router";

function guardRoute(to: any, from: any, next: any) {
  if (localStorage.id !== undefined) {
    next();
  } else {
    next("/login");
  }
}

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "home",
    component: () => import("../components/HomePage.vue"),
  },
  {
    path: "/register",
    name: "register",
    component: () => import("../components/SignUp.vue"),
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../components/Login.vue"),
  },
  {
    path: "/workout-plan-template",
    name: "workout-plan-template",
    beforeEnter: guardRoute,
    component: () => import("../components/WorkoutPlanTemplates.vue"),
  },
  {
    path: "/exercise",
    name: "exercise-form",
    beforeEnter: guardRoute,
    component: () => import("../components/ExerciseForm.vue"),
  },
  {
    path: "/exercise/:id",
    name: "exercise-detail",
    beforeEnter: guardRoute,
    component: () => import("../components/Exercise.vue"),
  },
  {
    path: "/complete-training",
    name: "complete-training",
    beforeEnter: guardRoute,
    component: () => import("../components/CompleteTraining.vue"),
  },
  {
    path: "/training-history",
    name: "training-history",
    beforeEnter: guardRoute,
    component: () => import("../components/TrainingHistory.vue"),
  },
  {
    path: "/completed-training/:id",
    name: "completed-training",
    beforeEnter: guardRoute,
    component: () => import("../components/CompletedTrainingDetails.vue"),
  },
  {
    path: "/workout-plan-template-details/:id",
    name: "workout-plan-template-details",
    beforeEnter: guardRoute,
    component: () => import("../components/WorkoutPlanTemplateDetails.vue"),
  },
  {
    path: "/trainer-list",
    name: "trainer-list",
    beforeEnter: guardRoute,
    component: () => import("../components/TrainersList.vue"),
  },
  {
    path: "/training-requests",
    name: "training-requests",
    beforeEnter: guardRoute,
    component: () => import("../components/TraineesRequests.vue"),
  },
  {
    path: "/trainees-feed",
    name: "trainees-feed",
    beforeEnter: guardRoute,
    component: () => import("../components/TraineesFeed.vue"),
  },
  {
    path: "/users-list",
    name: "users-list",
    beforeEnter: guardRoute,
    component: () => import("../components/UsersList.vue"),
  },
  {
    path: "/create-workout-plan-template",
    name: "create-workout-plan-template",
    component: () => import("../components/CreateWorkoutPlanTemplate.vue"),
  },
  {
    path: "/pupils-list",
    name: "pupils-list",
    component: () => import("../components/Pupils.vue"),
  },
  {
    path: "/workout-plan",
    name: "workout-plan",
    component: () => import("../components/WorkoutPlan.vue"),
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});
export default router;
