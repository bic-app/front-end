import http from "@/system/http-commons";
import CompletedTraining from "@/types/CompletedTraining";
import TrainingComment from "@/types/TrainingComment";

class StatsService {
  saveCompletedTraining(completedTraining: CompletedTraining) {
    return http.post("/completed-training", completedTraining);
  }

  fetchCompletedTraining(id: number) {
    return http.get("/completed-training", { params: { id } });
  }

  fetchTrainingHistory() {
    return http.get("/training-history");
  }
  fetchFeed() {
    return http.get("/feed");
  }

  getTrainingTypes() {
    return http.get("/training-types");
  }

  getFatigueLevels() {
    return http.get("/fatigue-levels");
  }
  addComment(id: number, comment: TrainingComment) {
    return http.post(`/comment`, comment, { params: { id } });
  }
}

export default new StatsService();
