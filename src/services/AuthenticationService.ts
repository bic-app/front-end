import http from "@/system/http-commons";
import User from "@/types/User";

class AuthenticationService {
  register(user: User) {
    return http.post("/register", user);
  }

  login(user: User) {
    return http.post("/login", user);
  }
  loginGoogle(code: string) {
    return http.post("/login/google", code);
  }
  registerGoogle(code: string) {
    return http.post("/register/google", code);
  }

  signOut() {
    return http.post("/sign-out", null);
  }
}

export default new AuthenticationService();
