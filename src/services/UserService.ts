import http from "@/system/http-commons";

class UserService {
  getUserInfo() {
    return http.get("/user-info");
  }

  fetchAllTrainers() {
    return http.get("/trainers");
  }

  fetchAllPupils() {
    return http.get("/pupils");
  }

  assignRole(role: string) {
    return http.put("/user/role", role);
  }
}

export default new UserService();
