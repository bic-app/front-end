import http from "@/system/http-commons";

class WorkoutDayService {
  fetchWorkoutDay(id: number) {
    return http.get(`/workout-day/${id}`);
  }
}

export default new WorkoutDayService();
