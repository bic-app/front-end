import http from "@/system/http-commons";

class WorkoutPlanService {
  fetchAllWorkoutPlan() {
    return http.get("/workout-plan");
  }
}

export default new WorkoutPlanService();
