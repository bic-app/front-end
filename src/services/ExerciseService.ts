import http from "@/system/http-commons";
import Exercise from "@/types/Exercise";

class ExerciseService {
  createExercise(exercise: Exercise) {
    return http.post("/exercise", exercise);
  }

  fetchExercise(id: number) {
    return http.get("/exercise", { params: { id } });
  }

  fetchAllExercises() {
    return http.get("/exercises");
  }

  getComplexity() {
    return http.get("/exercise/complexity");
  }
}

export default new ExerciseService();
