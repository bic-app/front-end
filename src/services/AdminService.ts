import http from "@/system/http-commons";
import User from "@/types/User";

class AdminService {
  fetchAllUsers() {
    return http.get("/admin/users");
  }
  updateUser(userInfo: User) {
    return http.post("/admin/user", { ...userInfo });
  }
  deleteUser(id: number) {
    return http.delete("/admin/user", { params: { id } });
  }
}
export default new AdminService();
