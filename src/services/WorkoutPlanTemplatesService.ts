import http from "@/system/http-commons";
import WorkoutPlanTemplate from "@/types/WorkoutPlanTemplate";

class WorkoutPlanTemplatesService {
  fetchAllWorkoutPlanTemplates() {
    return http.get("/workout-plan-template");
  }
  fetchAllWorkoutPlanTemplate(id: number) {
    return http.get("/workout-plan-template/" + id);
  }
  save(workoutPlanTemplate: WorkoutPlanTemplate) {
    return http.post("/workout-plan-template", workoutPlanTemplate);
  }
  assignToUser(id: number, workoutPlanTemplateId: number) {
    return http.post(
      `/workout-plan-template/${id}/user`,
      workoutPlanTemplateId
    );
  }
}

export default new WorkoutPlanTemplatesService();
