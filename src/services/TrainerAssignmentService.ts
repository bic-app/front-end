import http from "@/system/http-commons";

class TrainerAssignmentService {
  requestTrainer(trainerId: number) {
    return http.post("/request-trainer", trainerId);
  }
  fetchTrainingRequest() {
    return http.get("/training-requests");
  }
  acceptTrainee(trainingRequestId: number) {
    return http.put(`/accept-trainee/${trainingRequestId}`);
  }
  rejectTrainee(trainingRequestId: number) {
    return http.put(`/reject-trainee/${trainingRequestId}`);
  }
}

export default new TrainerAssignmentService();
