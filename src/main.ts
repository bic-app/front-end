import { createApp } from "vue";
import App from "./App.vue";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import router from "@/routes/routes";
import GAuth from "vue3-google-oauth2";
import { Plugin } from "@vue/runtime-core";

const gauthOption = {
  clientId:
    "1036670007008-q5m7om0digqm8sp80t9ljjul7t31khho.apps.googleusercontent.com",
  scope: "profile email",
  prompt: "select_account",
  plugin_name: "chat",
};
const app = createApp(App);
app.use(GAuth as unknown as Plugin, gauthOption);
app.use(router);
app.mount("#app");
