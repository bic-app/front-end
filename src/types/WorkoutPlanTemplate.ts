import CompletedExercise from "@/types/CompletedExercise";

interface WorkoutDay {
  exercises: CompletedExercise[];
}

export default interface WorkoutPlanTemplate {
  id: number | null;
  name: string;
  description: string;
  complexity: string;
  workoutDays: WorkoutDay[];
}
