export default interface User {
  id: null;
  username: string;
  password: string;
  enabled: boolean;
  authority: string[];
}
