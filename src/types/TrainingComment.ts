export default interface TrainingComment {
  id: null;
  comment: string;
  username: string;
  date: Date;
}
