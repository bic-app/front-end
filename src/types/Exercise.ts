export default interface Exercise {
  id: null | string;
  name: string;
  description: string;
  complexity: string;
  duration: number;
}
