import CompletedExercise from "@/types/CompletedExercise";
import TrainingComment from "@/types/TrainingComment";

export default interface CompletedTraining {
  id: null;
  type: string;
  completedAt: Date;
  timeInMillis: number;
  completedExercises: CompletedExercise[];
  comments: TrainingComment[];
  fatigueLevel: string;
}
