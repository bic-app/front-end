export default interface Complexity {
  value: string;
  displayName: string;
}
