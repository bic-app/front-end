export default interface CompletedExercise {
  id: null;
  exercise_definition_id: number;
  reps: number;
}
